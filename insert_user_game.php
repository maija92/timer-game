<?php
	include 'config.php';
	date_default_timezone_set('Europe/Riga');
	
    if (isset($_POST['data'])) {
		$startDatatime = date("Y-m-d H:i:s");		
		$endDatatime =  date("Y-m-d H:i:s", strtotime("+1 hours"));
		
		$sqlInsertGame = "INSERT INTO game (user_id, start_datetime, end_datetime, question)
		VALUES ('1','" . $startDatatime . "','" . $endDatatime . "', '" . $_POST['data']['question'] . "')";
		
		mysqli_query($con, $sqlInsertGame);
		
		$gameId = mysqli_insert_id($con);
		
		$sqlInsertGameResults = "INSERT INTO game_results (user_id, game_id, result)
		VALUES ('1','" . $gameId . "','" . $_POST['data']['answer'] . "')";
		
		mysqli_query($con, $sqlInsertGameResults);
		
		setcookie('gameId', $gameId, time() + (3600), "/");
	}	
?>
